# Query Streamer
Stream a potentially infinite number of records from a **relational** database without causing an [`OutOfMemoryError`][oom-error]. Compatible with Java 14.

### Usage
Given a [`DataSource`][datasource] `dataSource`,
```java
QueryStreamer queryStreamer = new QueryStreamer(dataSource);

Stream<String> orderIds = queryStreamer.streamRecords(
    "SELECT * FROM orders;",
    rs -> rs.getString("order_id")
);
```

### Gradle sub-project summary
| Module                  | Row converter                         |
|-------------------------|---------------------------------------|
| `query-streamer-spring` | [`ResultSetExtractor<T>`][spring-rse] |

## Build
Run `gradlew build`.

[oom-error]: https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/OutOfMemoryError.html
[spring-rse]: https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/jdbc/core/ResultSetExtractor.html
[datasource]: https://docs.oracle.com/en/java/javase/14/docs/api/java.sql/javax/sql/DataSource.html