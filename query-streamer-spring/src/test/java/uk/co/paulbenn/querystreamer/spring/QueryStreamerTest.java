package uk.co.paulbenn.querystreamer.spring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TestJdbcApplication.class)
@ActiveProfiles("test")
public class QueryStreamerTest {

    @Autowired
    private DataSource dataSource;

    @Test
    public void streamRecords() throws SQLException {
        QueryStreamer qs = new QueryStreamer(dataSource);

        List<Row> rows = qs.streamRecords("SELECT * FROM RECORDS;", rs -> new Row(rs.getString("id")))
            .collect(Collectors.toList());

        // see data.sql
        assertThat(rows.size()).isEqualTo(10);
        assertThat(rows).containsExactly(
            new Row("record-0"),
            new Row("record-1"),
            new Row("record-2"),
            new Row("record-3"),
            new Row("record-4"),
            new Row("record-5"),
            new Row("record-6"),
            new Row("record-7"),
            new Row("record-8"),
            new Row("record-9")
        );
    }

    private static class Row {
        String id;

        public Row(String id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Row row = (Row) o;
            return Objects.equals(id, row.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
