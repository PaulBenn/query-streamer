package uk.co.paulbenn.querystreamer.spring;

import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class QueryStreamer {

    private static final int FETCH_SIZE = 5000;

    private final DataSource dataSource;

    public QueryStreamer(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> Stream<T> streamRecords(String sql, ResultSetExtractor<T> extractor)
        throws SQLException {
        // A "linked list" of AutoCloseable elements is used throughout this method.
        // It may be easier to think of it as a 1-ary tree, with the "outer" AutoCloseable being the root.
        // Closing the "outer" AutoCloseable will close all children in order from first to last (or root to leaf).
        UncheckedCloseable closeableChain = null;

        try {
            // Open a database connection
            Connection connection = dataSource.getConnection();

            // AutoCloseable #1: Connection
            closeableChain = UncheckedCloseable.wrap(connection);

            // Pre-compile SQL, without running the result
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            // Set a reasonable number of rows retrieved per fetch operation
            preparedStatement.setFetchSize(FETCH_SIZE);

            // AutoCloseable #2: Connection -> PreparedStatement
            closeableChain = closeableChain.nest(preparedStatement);

            // Allow grouping multiple PreparedStatement instances into one commit
            connection.setAutoCommit(false);

            // Execute SQL (fetch the first FETCH_SIZE rows)
            ResultSet resultSet = preparedStatement.executeQuery();

            // AutoCloseable #3: Connection -> PreparedStatement -> ResultSet
            closeableChain = closeableChain.nest(resultSet);

            // Now that ResultSet is available for iteration, wrap it in a Stream
            return openStream(resultSet, extractor, closeableChain);
        } catch (SQLException e) {
            // If an exception occurs, close all resources in the chain before re-throwing
            if (closeableChain != null)
                try {
                    closeableChain.close();
                } catch (Exception any) {
                    e.addSuppressed(any);
                }
            throw e;
        }
    }

    private <T> Stream<T> openStream(ResultSet resultSet, ResultSetExtractor<T> extractor, UncheckedCloseable onClose) {
        return StreamSupport
            .stream(
                // Indicate unknown estimated size and pass default characteristics
                new Spliterators.AbstractSpliterator<T>(Long.MAX_VALUE, Spliterator.ORDERED) {
                    @Override
                    public boolean tryAdvance(Consumer<? super T> action) {
                        try {
                            // Advance the database cursor to the next row
                            if (!resultSet.next()) {
                                // No more rows to process
                                return false;
                            }

                            // Consume the row, and advance the stream cursor to the next element
                            action.accept(extractor.extractData(resultSet));
                            return true;
                        } catch (SQLException ex) {
                            // No checked operations allowed in streams
                            throw new RuntimeException(ex);
                        }
                    }
                },
                false)
            .onClose(onClose); // Ensure all resources are closed once stream is fully consumed
    }

    private interface UncheckedCloseable extends Runnable, AutoCloseable {

        // Intuitively, when this AutoCloseable Runnable runs, it should close.
        // Make sure it does so without throwing a checked exception so that we can use it in lambdas and streams.
        @Override
        default void run() {
            try {
                close();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        static UncheckedCloseable wrap(AutoCloseable c) {
            return c::close;
        }

        default UncheckedCloseable nest(AutoCloseable c) {
            return () -> {
                // The "outer" parameter is NOT unused. The try-with-resources block is capable of automatically closing
                // any resource within the brackets once the content of the try block executes.
                // For our use case, this has the effect of closing the nested resource recursively.
                try (UncheckedCloseable outer = this) {
                    c.close();
                }
            };
        }
    }
}
